from django.urls import path
import core.views.functional

app_name = 'functional'
urlpatterns = [
    path('view_1', core.views.functional.accident_count_view, name='view_1'),
    path('view_2', core.views.functional.applicant_phone_number_view, name='view_2'),
    path('view_3', core.views.functional.redirect_src_view, name='view_3'),
    path('redirect_dst', core.views.functional.redirect_dst_view, name='redirect_dst'),
    path('view_4', core.views.functional.rq_echo_view, name='view_4'),
    path('view_5', core.views.functional.user_data_by_phone_view, name='view_5'),
    path('view_6/<int:uid>', core.views.functional.user_json, name='view_6'),

    path('all_applicants', core.views.functional.all_applicants_view, name='all_applicants'),
    path('all_applicants_numbered', core.views.functional.all_applicants_numbered_view, name='all_applicants_numbered'),
    path('all_appeals', core.views.functional.all_appeals_view, name='all_appeals'),

    path('add_appeal', core.views.functional.add_appeal, name='add_appeal'),
    path('add_applicant', core.views.functional.add_applicant, name='add_applicant'),
    path('add_service', core.views.functional.add_service, name='add_service'),

    path('edit_service/<int:pk>', core.views.functional.edit_service, name='edit_service'),
    path('edit_applicant/<int:pk>', core.views.functional.edit_applicant, name='edit_applicant'),
    path('edit_appeal/<int:pk>', core.views.functional.edit_appeal, name='edit_appeal'),

    path('filter_applicant', core.views.functional.filter_applicant, name='filter_applicant'),
    path('filter_appeal', core.views.functional.filter_appeal, name='filter_appeal'),
    path('filter_applicant_name', core.views.functional.filter_applicant_activity, name='filter_applicant_name'),

    path('', core.views.functional.index_view, name='index'),
    path('footer', core.views.functional.footer_view, name='footer'),
    ]
