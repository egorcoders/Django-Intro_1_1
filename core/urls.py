from django.urls import path, include
import core.views.classed

app_name = 'core'
urlpatterns = [
    path('functional/', include('core.urls_functional', namespace='functional')),

    path('view_1', core.views.classed.AccidentCountView.as_view(), name='view_1'),
    path('view_2', core.views.classed.ApplicantPhoneNumberView.as_view(), name='view_2'),
    path('view_3', core.views.classed.RedirectSrcView.as_view(), name='view_3'),
    path('redirect_dst', core.views.classed.RedirectDstView.as_view(), name='redirect_dst'),
    path('view_4', core.views.classed.RequestEchoView.as_view(), name='view_4'),
    path('view_5', core.views.classed.UserDataByPhoneView.as_view(), name='view_5'),
    path('view_6/<int:uid>', core.views.classed.UserJsonView.as_view(), name='view_6'),

    path('all_applicants', core.views.classed.AllApllicantsView.as_view(), name='all_applicants'),
    path('all_applicants_numbered', core.views.classed.AllApllicantsNumberedView.as_view(), name='all_applicants_numbered'),
    path('all_appeals', core.views.classed.AllAppealsView.as_view(), name='all_appeals'),

    path('add_service', core.views.classed.AddServiceView.as_view(), name='add_service'),
    path('add_applicant', core.views.classed.AddApplicantView.as_view(), name='add_applicant'),
    path('add_appeal', core.views.classed.AddAppealView.as_view(), name='add_appeal'),

    path('edit_service/<int:pk>', core.views.classed.EditServiceView.as_view(), name='edit_service'),
    path('edit_applicant/<int:pk>', core.views.classed.EditApplicantView.as_view(), name='edit_applicant'),
    path('edit_appeal/<int:pk>', core.views.classed.EditAppealView.as_view(), name='edit_appeal'),

    path('filter_applicant', core.views.classed.FilterApplicantView.as_view(), name='filter_applicant'),
    path('filter_appeal', core.views.classed.FilterAppealView.as_view(), name='filter_appeal'),
    path('filter_applicant_name', core.views.classed.FilterApplicantNameView.as_view(), name='filter_applicant_name'),

    path('', core.views.classed.IndexView.as_view(), name='index'),
    path('footer', core.views.classed.FooterView.as_view(), name='footer'),
    path('success', core.views.classed.SuccessView.as_view(), name='success'),
    ]
